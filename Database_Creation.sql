DROP TABLE IF EXISTS Emp;
DROP TABLE IF EXISTS Ticket;
DROP TABLE IF EXISTS Show_DB;
DROP TABLE IF EXISTS Expense;

CREATE TABLE Emp (
       Emp_Name  VARCHAR (20),
       Emp_ID  VARCHAR (10),
       Passwrd  VARCHAR (20) DEFAULT 'firstlogin',
       Ph_No  VARCHAR (10),
       Address  VARCHAR (100),
       Emp_Type  VARCHAR(15) DEFAULT 'Salesman',
       Status  VARCHAR(12) DEFAULT 'Activated',
       Sales  INTEGER DEFAULT '0',
       PRIMARY KEY (Emp_ID)
);

CREATE TABLE Ticket (
       Ticket_ID  VARCHAR(20),
       Show_Name  VARCHAR(15),
       Show_ID  VARCHAR(25),
       Emp_ID  VARCHAR(10),
       No_Spect SMALLINT,
       Name_Spect  VARCHAR (200),
       Age_Spect  VARCHAR (30),
       Seat_Spect  VARCHAR (40),
       Ticket_Status  VARCHAR(10) DEFAULT 'Booked',
       Total INTEGER,
       PRIMARY KEY (Ticket_ID)
);

CREATE TABLE Show_DB (
       Name  VARCHAR(15),
       Show_ID  VARCHAR(25),
       Date  DATE,
       Start_Time  TIME,
       End_Time  TIME,
       Dur  INTEGER,
       No_Bal  INTEGER,
       No_Stall  INTEGER,
       Price_Bal  INTEGER,
       Price_Stall  INTEGER,
       Seat_Status  VARCHAR(200),
       Seat_Cancelled  VARCHAR(200) DEFAULT '0|0|0|0|0|0',
       Profit INTEGER DEFAULT '0',
       Expenses INTEGER DEFAULT '0',
       Year INTEGER,
       PRIMARY KEY (Show_ID)
);

CREATE TABLE Expense (
       No INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
       Name  VARCHAR(15),
       Amount  INTEGER,
       Expense  VARCHAR(20),
       PRIMARY KEY (No)
);

INSERT INTO Emp (Emp_Name, Emp_ID, Ph_No, Address,Emp_Type)
       VALUES  	('Varun', 'EMP01','123','varun','manager'),
               	('Tejas', 'EMP02','123','tejas','accounts_person');
               	