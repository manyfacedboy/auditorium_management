
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Teju
 */
public class Employee {
   protected String Name;
   protected String EmpID;
   protected String Password;
   protected String type;
   protected Connection conn=null;
   Employee(String name,String EmpID,String type){
       this.Name=name;
       this.EmpID=EmpID;
       this.type=type;
   }
   public String getName(){
       return Name;
   }
   public String getEmpID(){
       return EmpID;
   }
   public String Password(){
       return Password;
   }
   public String gettype(){
       return this.type;
   }
   public static Employee getEmpfromDB(String EmpID){
       try{
       Connection conn=javaconnect.ConnectDB();
       String Query="SELECT * FROM emp WHERE Emp_ID = ?;";
       PreparedStatement st=conn.prepareStatement(Query);
       st.setString(1,EmpID);
       ResultSet rs=st.executeQuery();
       rs.first();
       System.out.println("yo3");
       if(rs.getRow()>0){
           String Type=rs.getString("Emp_Type");
           if(Type.equalsIgnoreCase("salesperson")){
               System.out.println("LOL");
              SalesPerson sm=new SalesPerson(rs.getString("Emp_Name"),rs.getString("Emp_ID"),rs.getInt("Sales"),rs.getString("Status"));
              return sm;
           }
           else if(Type.equalsIgnoreCase("manager")){
             Manager mn=new Manager(rs.getString("Emp_Name"),rs.getString("Emp_ID"));
             return mn;
           }
           else if(Type.equalsIgnoreCase("accounts_person")){
               Accountant ap=new Accountant(rs.getString("Emp_Name"),rs.getString("Emp_ID"));
               return ap;
           }
           return null;
       }
       else{
           System.out.println("Employee not found");
           return null;
       }
       }catch(Exception e){
           System.out.println(e);
           return null;
       }     
   }
   public static boolean verifyPassword(String EmpID,String passwrd){
       try{
       Connection conn=javaconnect.ConnectDB();    
       //Statement st=conn.createStatement();
       System.out.println("yo1");
       String Query="select * from emp where Emp_ID=? and Passwrd=?";
       PreparedStatement st=conn.prepareStatement(Query);
       st.setString(1, EmpID);
       st.setString(2, passwrd);
       ResultSet rs=st.executeQuery();
       System.out.println("yo");
       if(rs.next())
           return true;
       else
          return false;
       }
       catch(Exception e){
           System.out.println(e);
           return false;
       }
   }
   public boolean changePassword(String Oldpass,String newpass){
       if(verifyPassword(EmpID,Oldpass)){
       try{
       conn=javaconnect.ConnectDB(); 
       Statement st=conn.createStatement();
       String Query = "Update emp SET Passwrd ='"+newpass+"'WHERE Emp_ID='"+EmpID+"'";
       st.executeUpdate(Query);
       return true;
       }catch(Exception e){
           System.out.println(e);
           return false;
       }
       }
       else
        return false;
   }
}
