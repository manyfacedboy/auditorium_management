
import com.mysql.jdbc.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.StringTokenizer;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Teju
 */
public class Manager extends Employee {
    Manager(String name,String EmpID){
        super(name,EmpID,"manager");
    }
    String addSalesPerson(String name,String phno, String addr){
        try{
        int EmpID=SalesPerson.getId();
        System.out.println(EmpID);
        String empid;
        if(EmpID<10)
        empid="EMP0"+EmpID;
        else
        empid="EMP"+EmpID;
        Connection conn=javaconnect.ConnectDB();
        
        String Query = "Insert Into emp (Emp_Name, Emp_ID, Ph_No, Address,Emp_Type) Value(?,?,?,?,?)";
        PreparedStatement st=conn.prepareStatement(Query);
        st.setString(1,name);
        st.setString(2, empid);
        st.setString(3, phno);
        st.setString(4,addr);
        st.setString(5, "salesperson");
            st.execute();
            return empid;
        }catch(Exception e){
            System.out.println(e);
            return null;
        }
    }
    boolean removeSalesPerson(String empid){
        try{
        Connection conn=javaconnect.ConnectDB();
        String Query = "UPDATE emp SET Status='"+"deactivated"+"' WHERE Emp_ID='"+empid+"' ";
        PreparedStatement st=conn.prepareStatement(Query);
        st.execute();
        return true;
        }catch(Exception e){
            System.out.println(e);
            return false;
        }
    }
    boolean addShow(Show show){
        try{
        int te1,te2,te3,te4,te5,te6,te7,te8;
        String s1,s2;
        boolean con1,con2,con3,con4,con;
        Connection conn=javaconnect.ConnectDB();
        String Query ="SELECT * FROM show_db WHERE Date =?";
        PreparedStatement st=conn.prepareStatement(Query);
        st.setString(1, show.dt);
        ResultSet rs=st.executeQuery();
        te1=Integer.parseInt(show.stime.substring(0,1));
        te2=Integer.parseInt(show.stime.substring(2,3));
        te3=Integer.parseInt(show.etime.substring(0,1));
        te4=Integer.parseInt(show.etime.substring(2,3));
        while(rs.next()){
            s1=rs.getString("Start_Time");
            s2=rs.getString("End_Time");
            te5=Integer.parseInt(s1.substring(0,1));
            te6=Integer.parseInt(s1.substring(2,3));
            te7=Integer.parseInt(s2.substring(0,1));
            te8=Integer.parseInt(s2.substring(2,3));
            con1=(te1>te5)||(te1==te5 && te2>=te6);
            con2=(te1<te7)||(te1==te7 && te2<=te8);
            con3=(te3>te5)||(te3==te5 && te4>=te6);
            con4=(te3<te7)||(te3==te7 && te4<=te8);
            con=(con1&& con2) || (con3 && con4);
            if(con){
               System.out.println("lol");
               return false;
            }
        }
        String seat_s="";
        int i;
        for(i=0;i<50;i++)
           seat_s+="1";
        for(i=0;i<50;i++)
            seat_s+="2";
        for(i=0;i<50;i++)
            seat_s+="1";
        for(i=0;i<50;i++)
            seat_s+="2";
       Query = "Insert Into show_db (Name,Show_ID, Date, Start_Time, End_Time,Dur,No_Bal,No_Stall,Price_Bal,Price_Stall,Seat_Status ) Value(?,?,?,?,?,?,?,?,?,?,?)";
       st=conn.prepareStatement(Query);
       st.setString(1, show.name);
       st.setString(2,show.show_id);
       st.setString(3, show.dt);
       st.setString(4,show.stime);
       st.setString(5,show.etime);
       st.setInt(6,show.dur);
       st.setInt(7,show.noBalSeats);
       st.setInt(8,show.noStallSeats);
       st.setInt(9,show.balPrice);
       st.setInt(10,show.StallPrice);
       st.setString(11,seat_s);
       st.execute();
       return true;
      } catch(Exception e){
          e.printStackTrace();
          return false;
      }
    }
    int[] showCollection(String showid){
        try{
        int[] res=new int[12];
        int j;
        Connection conn=javaconnect.ConnectDB();
        String Query="SELECT * FROM show_db WHERE Show_ID =?";
        PreparedStatement st=conn.prepareStatement(Query);
        st.setString(1, showid);
        ResultSet rs=st.executeQuery();
        rs.first();
        String seat=rs.getString("Seat_Status");
        for(int i=0;i<seat.length();i++){
            j=seat.charAt(i)-'0';
            if(i<100){
               if(j==0)  ++res[0];
               if(j==1)  ++res[1];
               if(j==2)  ++res[2];
            }
            else{
               if(j==0)  ++res[3];
               if(j==1)  ++res[4];
               if(j==2)  ++res[5];
            }
        }
        String cancel=rs.getString("Seat_Cancelled");
        StringTokenizer stz=new StringTokenizer(cancel,"|");
        for(int i=0;i<6;i++)
            System.out.println(res[i]);
        int i=6;
        while(stz.hasMoreTokens()){
            res[i]=Integer.parseInt(stz.nextToken());
            ++i;
        }
        for(i=6;i<12;i++)
            System.out.println(res[i]);
        return res;
      }catch(Exception e){
          System.out.println(e);
          return null;
      }
    }     
      Object[][] viewEmpSales(String EmpID){
        try{
         Object[][] obj; 
         Connection conn=javaconnect.ConnectDB();
         String Query="SELECT * FROM ticket WHERE Emp_ID =?";
         PreparedStatement st=conn.prepareStatement(Query);
         st.setString(1,EmpID);
         ResultSet rs=st.executeQuery();
         rs.last();
         int size = rs.getRow();
         rs.beforeFirst();
         obj=new Object[size][4];
         int i=0;
         System.out.println(size+"lol");
         while(rs.next()){
            obj[i][0]=rs.getString("Ticket_ID");
            obj[i][1]=rs.getString("Show_ID");
            obj[i][2]=rs.getInt("Total");
            obj[i][3]=rs.getString("Ticket_Status");
         }
  
        return obj;
        }catch(Exception e){
    System.out.println(e);
    return null;
}
    }
   Object[][] showBalSheet(Show show){
       try{
       String seat,cancel;
       StringTokenizer stz;
       int j;
       int[] can=new int[6];
       int bal_book=0,stall_book=0;
       Object[][] expen;
       Connection conn=javaconnect.ConnectDB();
       String Query="SELECT * FROM expense WHERE Name =?";
       PreparedStatement st=conn.prepareStatement(Query);
       st.setString(1, show.name);
       ResultSet rs=st.executeQuery();
       rs.last();
       int size = rs.getRow();
       rs.beforeFirst();
       expen=new Object[size+8][3];
       int i=8;
       while(rs.next()){
           expen[i][0]=rs.getString("Expense");
           expen[i][1]=rs.getString("Amount");
           i++;
       }
       Query="SELECT * FROM show_db WHERE Name =? ";
       st=conn.prepareStatement(Query);
       st.setString(1, show.name);
       rs=st.executeQuery();
       while(rs.next()){
           seat=rs.getString("Seat_Status");
        for(i=0;i<seat.length();i++){
            j=seat.charAt(i)-'0';
            if(i<200){
               if(j==1)  ++bal_book;
            }
            else{
               if(j==1)  ++stall_book;
            }
        }
        cancel=rs.getString("Seat_Cancelled");
        stz=new StringTokenizer(cancel,"|");
        i=0;
        while(stz.hasMoreTokens()){
            can[i]=Integer.parseInt(stz.nextToken());
            ++i;
        }  
       }
       expen[0][0]="Balcony booked"+bal_book;
       expen[0][1]=bal_book*show.balPrice;
       expen[1][0]="Stall booked"+stall_book;
       expen[1][1]=stall_book*show.StallPrice;
       expen[2][0]="Cancellation Balcony-before 3days"+can[0];
       expen[2][1]=can[0]*5;
       expen[3][0]="Cancellation Balcony-before 3days"+can[0];
       expen[3][1]=can[1]*15;
        expen[4][0]="Cancellation Balcony-before 3days"+can[0];
       expen[4][1]=can[2]*.5*show.balPrice;
        expen[5][0]="Cancellation Balcony-before 3days"+can[0];
       expen[5][1]=can[3]*5;
        expen[6][0]="Cancellation Balcony-before 3days"+can[0];
       expen[6][1]=can[4]*10;
        expen[7][0]="Cancellation Balcony-before 3days"+can[0];
       expen[7][1]=can[5]*.5*show.StallPrice;
       for(i=0;i<size+8;i++)
           System.out.println(expen[i][1]);
       return expen;
       }catch(Exception e){
           e.printStackTrace();
           return null;
       }
   }
   Object[][] yearBalSheet(int year){
       try{
       Connection conn=javaconnect.ConnectDB();
       int i,j;
       String name;
       boolean flag;
       String Query="SELECT * FROM show_db WHERE Year =?";
       PreparedStatement st=conn.prepareStatement(Query);
       st.setInt(1,year);
       ResultSet rs=st.executeQuery();
       Object[][] res=new Object[100][2];
       i=0;
       while(rs.next()){
           System.out.println("1");
           name=rs.getString("Name");
           flag=true;
          for(j=0;j<i;j++){
              if(res[j][0]==name){
                 flag=false;
                 break;
              }
          }
          if(flag){
             res[i][0]=name;
             res[i][1]=rs.getInt("Profit");
          }
          i++;
          System.out.println(i+"lol");
       }
       for(j=0;j<i;j++)
           System.out.println(res[j][1]);
       return res;
       }catch(Exception e){
           System.out.println(e);
           return null;
       }
   }
   ArrayList<String> listsalesperson(){
       try{
       Connection conn=javaconnect.ConnectDB();
       String Query="select * from emp where Type=?";
       PreparedStatement st=conn.prepareStatement(Query);
       st.setString(1,"salesperson");
       ArrayList<String> list=new ArrayList<String>();
       ResultSet rs=st.executeQuery();
       while(rs.next()){
           list.add(rs.getString("Emp_Name"));
       }
       return list;
       }catch(Exception e){
           e.printStackTrace();
           return null;
       }
   }
    
}
