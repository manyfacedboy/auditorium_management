
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Calendar;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Teju
 */
public class Show {
    String name;
    String show_id;
    String stime,etime;
    String dt;
    int dur;
    int noBalSeats;
    int noStallSeats;
    int balPrice;
    int StallPrice;
    int profit;
    int expenses;
    int year;
    Connection conn=null;
    Show(String name,String description,int time, int balprice,int stallprice){
        this.name=name;
        this.balPrice=balprice;
        this.StallPrice=stallprice;
        this.dur=time;
    }
    Show(String name,String show_id,String stime,String etime,String dt,int dur,int noBalSeats,int noStallSeats,int balPrice,int stallPrice,int year)
    {
    this.name=name;
    this.show_id=show_id;
    this.stime=stime;
    this.etime=etime;
    this.dt=dt;
    this.dur=dur;
    this.noBalSeats=noBalSeats;
    this.noStallSeats=noStallSeats;
    this.balPrice=balPrice;
    this.StallPrice=StallPrice;
    this.year=year;
    }
    static ArrayList<String> getShows(){
        try{
        Connection conn=javaconnect.ConnectDB();
        Statement st=conn.createStatement();
        int i=-1;
        ArrayList<String> sh=new ArrayList<String>();
        String Query="SELECT * FROM show_db;";
        ResultSet rs=st.executeQuery(Query);
        while(rs.next()){
            if((sh.get(i)).equalsIgnoreCase(rs.getString("show")) && i!=-1)
                continue;
            sh.add(rs.getString("show"));
            i++;
        }
        return sh;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
    static ArrayList<String> getDates(String sh){
        try{
        Connection conn=javaconnect.ConnectDB();
        ArrayList<String> date=new ArrayList<String>();
        int i=-1;
        String Query="select * from show_db order by date where show=?";
        PreparedStatement st=conn.prepareStatement(Query);
        st.setString(1, sh);
        ResultSet rs=st.executeQuery();
        while(rs.next()){
           if((date.get(i)).equalsIgnoreCase(rs.getString("date")) && i!=-1)
                continue;
            date.add(rs.getString("date"));
            i++; 
        }
        return date;
        }catch(Exception e){
           e.printStackTrace();
           return null;
        }
    }
    static ArrayList<String> getTime(String sh,String date){
       try{
        Connection conn=javaconnect.ConnectDB();
        ArrayList<String> time=new ArrayList<String>();
        int i=-1;
        String Query="select * from show_db order by date where show=? and date=?";
        PreparedStatement st=conn.prepareStatement(Query);
        st.setString(1, sh);
        st.setString(2, date);
        ResultSet rs=st.executeQuery();
        while(rs.next()){
           time.add(rs.getString("time"));
        }
        return time;
        }catch(Exception e){
           e.printStackTrace();
           return null;
        } 
    }
}
