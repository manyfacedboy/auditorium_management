
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.StringTokenizer;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Teju
 */
public class SalesPerson extends Employee{
    int Sales;
    String status;
    SalesPerson(String name,String EmpID){
        super(name,EmpID,"salesperson");
        try{
        Connection conn=javaconnect.ConnectDB();
        String Query="select * from emp where Emp_ID=?";
        PreparedStatement st=conn.prepareStatement(Query);
        st.setString(1,EmpID);
        ResultSet rs=st.executeQuery();
        rs.next();
        this.Sales=rs.getInt("Sales");
        this.status=rs.getString("Status");
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    SalesPerson(String name,String EmpID, int sales, String status){
        super(name,EmpID,"salesperson");
        this.Sales=sales;
        this.status=status;
    }
    static int getId(){
        try{
        Connection conn=javaconnect.ConnectDB();
        Statement st=conn.createStatement();
        String Query = "SELECT * FROM emp;";
        ResultSet rs = st.executeQuery(Query);
        rs.last();
        String str=rs.getString("Emp_ID");
        return(Integer.parseInt(str.substring(3))+1);
        }
       catch(Exception e){
           System.out.println(e);
           return -1;
       }
    }
    int bookticket(ArrayList<String> spect,Show show,ArrayList<Integer> age2,ArrayList<String> seats){
        try{
       Connection conn=javaconnect.ConnectDB();
       String Query1="select * from show_db where show_id=?";
       PreparedStatement st1=conn.prepareStatement(Query1);
       ResultSet rs1=st1.executeQuery();
       String Show_id=rs1.getString("Show_ID");
       String seatmatrix=rs1.getString("Seat_Status");
       String dummy;
       String seat2;
       char alpha;
       int seatno;
       int id=Ticket.getTicketID();
       int i,no;
       int cost=0;
       String name="";
       String age1="";
       String seat1="";
       for( i=0;i<spect.size()-1;i++){
           name+=spect.get(i)+"|";
           age1+=String.valueOf(age2.get(i))+"|";
           seat2=seats.get(i);
           alpha=seat2.charAt(0);
           seatno=Integer.parseInt(seat2.substring(1));
           no=(alpha-'A')*30+seatno;
           if(no<180)
              cost+=rs1.getInt("Price_Bal");
           else
              cost+=rs1.getInt("Price_Stall");
           dummy=seatmatrix.substring(0, no-2)+'1'+seatmatrix.substring(no);
           seatmatrix=dummy;
           seat1+=seats.get(i)+"|";
       }
       name+=spect.get(i);
       age1+=String.valueOf(age2.get(i));
       seat1+=seats.get(i);
       String Query = "Insert Into ticket (Ticket_ID,Show_Name,Show_ID,Emp_ID,No_Spect,Name_Spect,Age_Spect,Seat_Spect,Total) Value(?,?,?,?,?,?,?,?,?)";
       PreparedStatement st=conn.prepareStatement(Query);
       st.setInt(1,id);
       st.setString(2,show.name);
       st.setString(3, show.show_id);
       st.setString(4, this.EmpID);
       st.setInt(5,spect.size());
       st.setString(6,name);
       st.setString(7,age1);
       st.setString(8,seat1);
       st.setInt(9,cost);
       st.execute();
       Query1="Update show_db SET Seat_Status = '"+seatmatrix+"' WHERE Show_ID='"+Show_id+"'";
       st1=conn.prepareStatement(Query1);
       st1.execute();
            return 1;
    }catch(Exception e){
        e.printStackTrace();
        return -1;
    }
}
/*int cancelTicket(String TicketID){
   try{
   Connection conn=javaconnect.ConnectDB();
   String Query="SELECT * FROM ticket WHERE Ticket_ID =? ";
   PreparedStatement st=conn.prepareStatement(Query);
   st.setString(1,TicketID);
   ResultSet rs=st.executeQuery();
   String show_id=rs.getString("Show_ID");
   String Query1="select * from show_db where Show_ID=?";
   PreparedStatement st1=conn.prepareStatement(Query1);
   st1.setString(1, show_id);
   ResultSet rs1=st1.executeQuery();
   String seatmatrix=rs1.getString("Seat_Status");
   String seat1;
   char alpha;
   int seatno,no,cancel=0;
   String seat=rs.getString("Seat_Spect");
   StringTokenizer tok=new StringTokenizer(seat,"|");
   while(tok.hasMoreTokens()){
       seat1=tok.nextToken();
       alpha=seat1.charAt(0);
       seatno=Integer.parseInt(seat1.substring(1));
       no=(alpha-'A')*30+seatno;
       if(no<180)
          cancel+=rs1.getInt("Price_Bal");
       else
          cancel+=rs1.getInt("Price_Stall");
          dummy=seatmatrix.substring(0, no-2)+'0'+seatmatrix.substring(no);
          seatmatrix=dummy;
   }
   Query = "Update ticket SET Ticket_Status = '"+"cancelled"+"' WHERE Ticket_ID='"+TicketID+"' ";
   st=conn.prepareStatement(Query);
   st.execute();
   System.out.println("HI");
   return 1;
   }catch(Exception e){
       System.out.println(e);
       return 0;
}
}*/
Object[][] viewSales(String EmpID){
        try{
         Object[][] obj; 
         Connection conn=javaconnect.ConnectDB();
         String Query="SELECT * FROM ticket WHERE Emp_ID =?";
         PreparedStatement st=conn.prepareStatement(Query);
         st.setString(1,EmpID);
         ResultSet rs=st.executeQuery();
         rs.last();
         int size = rs.getRow();
         rs.beforeFirst();
         obj=new Object[size][4];
         int i=0;
         System.out.println(size);
         while(rs.next()){
            obj[i][0]=rs.getString("Ticket_ID");
            obj[i][1]=rs.getString("Show_ID");
            obj[i][2]=rs.getInt("Total");
            obj[i][3]=rs.getString("Ticket_Status");
         }
        return obj;
        }catch(Exception e){
    System.out.println(e);
    return null;
}
    }
}
